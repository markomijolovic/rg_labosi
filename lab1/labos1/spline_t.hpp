#pragma once

#include <fstream>
#include <glm/glm.hpp>
#include <vector>

namespace detail {
const auto position_coefficients          = glm::mat4{-1, 3, -3, 1, 3, -6, 0, 4, -3, 3, 3, 1, 1, 0, 0, 0};
const auto tangent_coefficients           = glm::mat4x3{-1, 2, -1, 3, -4, 0, -3, 2, 1, 1, 0, 0};
const auto second_derivative_coefficients = glm::mat4x2{-1, 1, 3, -2, -3, 1, 1, 0};
}

struct spline_t {
    std::vector<glm::vec3> spline_points{};
    std::vector<glm::vec3> control_points{};
    std::vector<glm::vec3> tangents{};
    std::vector<glm::vec3> normals{};
    std::vector<glm::vec3> binormals{};
};

[[nodiscard]] inline auto load_spline(const std::string &filename) -> spline_t
{
    auto spline     = spline_t{};
    auto ifstream   = std::ifstream{filename};
    auto num_points = std::size_t{};
    ifstream >> num_points;

    spline.control_points.resize(num_points);
    for (auto &point: spline.control_points)
        ifstream >> point.x >> point.y >> point.z;

    const auto  num_segments = num_points - 3;
    const auto &points       = spline.control_points;

    for (auto current_segment = 1; current_segment <= num_segments; current_segment++) {
        const auto active_points = transpose(glm::mat4x3{points[current_segment - 1], points[current_segment], points[current_segment + 1], points[current_segment + 2]});

        for (auto t = 0.0F; t < 1.0F; t += 0.001F) {
            spline.spline_points.push_back(glm::vec4{(t * t * t), t * t, t, 1} / 6.0F * detail::position_coefficients * active_points);
            const auto tangent = normalize(glm::vec3{t * t, t, 1} / 2.0F * detail::tangent_coefficients * active_points);
            spline.tangents.push_back(tangent);
            const auto second_derivative = glm::vec2{t, 1} * detail::second_derivative_coefficients * active_points;
            const auto normal            = cross(tangent, normalize(second_derivative));
            spline.normals.push_back(normal);
            const auto binormal = cross(tangent, normal);
            spline.binormals.push_back(binormal);
        }
    }

    // add t==1.0F for last segment
    const auto current_segment = num_segments;
    const auto active_points   = transpose(glm::mat4x3{points[current_segment - 1], points[current_segment], points[current_segment + 1], points[current_segment + 2]});
    const auto t               = 1.0F;
    spline.spline_points.push_back(glm::vec4{(t * t * t), t * t, t, 1} / 6.0F * detail::position_coefficients * active_points);
    const auto tangent = normalize(glm::vec3{t * t, t, 1} / 2.0F * detail::tangent_coefficients * active_points);
    spline.tangents.push_back(tangent);
    const auto second_derivative = glm::vec2{t, 1} * detail::second_derivative_coefficients * active_points;
    const auto normal            = cross(tangent, normalize(second_derivative));
    spline.normals.push_back(normal);
    const auto binormal = cross(tangent, normal);
    spline.binormals.push_back(binormal);

    return spline;
}
