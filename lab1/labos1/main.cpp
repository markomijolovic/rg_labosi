#include "camera_t.hpp"
#include "mesh_t.hpp"
#include "spline_t.hpp"

#include <GL/glew.h>
#include <algorithm>
#include <chrono>
#include <glfw/glfw3.h>
#include <glm/ext.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/glm.hpp>
#include <iostream>
#include <unordered_map>

constexpr auto width  = 1280;
constexpr auto height = 720;

constexpr auto start_orientation = glm::vec3{0, 0, 1};
auto           first_mouse{true};
auto           x_offset{0.0F};
auto           y_offset{0.0F};
auto           x{0.0F};
auto           y{0.0F};
auto           diff_x       = int{};
auto           diff_y       = int{};
auto           dcm_matrix   = bool{};
auto           control_poly = bool{};

auto now        = std::chrono::high_resolution_clock::now();
auto delta_time = 0.0F;

auto camera = camera_t{
    glm::perspective(glm::radians(60.0F), static_cast<float>(width) / height, 0.01F, 1000.0F),
    {{0.0F, 0.0F, 0.0F, 1.0F}, {0, 180, 0}, {1.0F, 1.0F, 1.0F}}};

const auto mesh_spline_pairs = std::vector<std::pair<mesh_t, spline_t>>{
    {load_mesh("models/f16.obj"),
     load_spline("splines/test.txt")},
    {load_mesh("models/porsche.obj"),
     load_spline("splines/b.txt")}};

auto spline_indices = std::vector<float>(mesh_spline_pairs.size());

auto display() -> void
{
    if (glGetError() != GL_NO_ERROR) {
        std::cout << "ERROR" << std::endl;
    }
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    const auto view_matrix = camera.transform.get_view_matrix();
    glMultMatrixf(&view_matrix[0][0]);

    const auto draw_mesh = [](const auto &mesh, const auto &spline, auto point_index, const auto orientation) {
        const auto new_position = spline.spline_points[static_cast<std::size_t>(point_index)];

        glPushMatrix();

        glTranslatef(new_position.x, new_position.y, new_position.z);

        const auto tangent  = spline.tangents[static_cast<std::size_t>(point_index)];
        const auto normal   = spline.normals[static_cast<std::size_t>(point_index)];
        const auto binormal = spline.binormals[static_cast<std::size_t>(point_index)];

        if (dcm_matrix) {
            const auto rotation_matrix = glm::mat4{glm::vec4{tangent, 0}, glm::vec4{normal, 0}, glm::vec4{binormal, 0}, glm::vec4{0, 0, 0, 1}} * rotate(glm::mat4{1.0F}, glm::radians(90.0F), glm::vec3{0, 1, 0});
            glMultMatrixf(&rotation_matrix[0][0]);
        } else {
            const auto new_orientation = spline.tangents[static_cast<std::size_t>(point_index)];
            const auto rotation_axis   = normalize(glm::cross(orientation, new_orientation));
            const auto rotation_amount = std::acos(dot(new_orientation, orientation));
            glRotatef(glm::degrees(rotation_amount), rotation_axis.x, rotation_axis.y, rotation_axis.z);
        }

        glScalef(2.0F, 2.0F, 2.0F);
        for (const auto &face: mesh.faces) {
            const auto &v1 = mesh.vertices[face.x - 1];
            const auto &v2 = mesh.vertices[face.y - 1];
            const auto &v3 = mesh.vertices[face.z - 1];

            glBegin(GL_TRIANGLES);
            glVertex3f(v1.x, v1.y, v1.z);
            glVertex3f(v2.x, v2.y, v2.z);
            glVertex3f(v3.x, v3.y, v3.z);

            glEnd();
        }

        glPopMatrix();
    };

    const auto draw_spline = [](const auto &spline) {
        glBegin(GL_LINE_STRIP);
        for (const auto &point: spline.spline_points) {
            glVertex3f(point.x, point.y, point.z);
        }
        glEnd();

        glColor3f(1.0F, 0.0F, 0.0F);
        glBegin(GL_LINE_STRIP);
        if (control_poly)
            for (const auto &point: spline.control_points)
                glVertex3f(point.x, point.y, point.z);

        glEnd();
    };

    const auto draw_tangent = [](const auto &spline, auto point_index) {
        glBegin(GL_LINES);
        const auto &point     = spline.spline_points[static_cast<std::size_t>(point_index)];
        const auto &tangent   = spline.tangents[static_cast<std::size_t>(point_index)];
        const auto  new_point = point + tangent;
        glVertex3f(point.x, point.y, point.z);
        glVertex3f(new_point.x, new_point.y, new_point.z);
        glEnd();
    };

    const auto draw_binormal = [](const auto &spline, auto point_index) {
        glBegin(GL_LINES);
        const auto &point     = spline.spline_points[static_cast<std::size_t>(point_index)];
        const auto &tangent   = spline.binormals[static_cast<std::size_t>(point_index)];
        const auto  new_point = point + tangent;
        glVertex3f(point.x, point.y, point.z);
        glVertex3f(new_point.x, new_point.y, new_point.z);
        glEnd();
    };

    const auto draw_normal = [](const auto &spline, auto point_index) {
        glBegin(GL_LINES);
        const auto &point     = spline.spline_points[static_cast<std::size_t>(point_index)];
        const auto &tangent   = spline.normals[static_cast<std::size_t>(point_index)];
        const auto  new_point = point + tangent;
        glVertex3f(point.x, point.y, point.z);
        glVertex3f(new_point.x, new_point.y, new_point.z);
        glEnd();
    };

    for (std::size_t i = 0; i < spline_indices.size(); i++) {
        const auto &[mesh, spline] = mesh_spline_pairs[i];
        const auto point_index     = spline_indices[i];
        glColor3f(0.25F, 0.25F, 0.25F);
        draw_mesh(mesh, spline, point_index, start_orientation);
        glColor3f(0.0F, 0.0F, 0.0F);
        draw_spline(spline);
        glColor3f(0.0F, 0.0F, 1.0F);
        draw_tangent(spline, point_index);
        if (dcm_matrix) {
            glColor3f(0.0F, 1.0F, 0.0F);
            draw_normal(spline, point_index);
            glColor3f(1.0F, 0.0F, 0.0F);
            draw_binormal(spline, point_index);
        }
    }
}

auto buttons = std::unordered_map<int, bool>{};

auto key_callback(GLFWwindow *window, int key, int /*scan_code*/, int action, int /*mode*/) -> void
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, 1);
        return;
    }

    if (key == GLFW_KEY_R && action == GLFW_PRESS)
        dcm_matrix = !dcm_matrix;

    if (key == GLFW_KEY_P && action == GLFW_PRESS)
        control_poly = !control_poly;

    if (action == GLFW_PRESS)
        buttons[key] = true;
    else if (action == GLFW_RELEASE)
        buttons[key] = false;
}

auto process_input(float delta_time) -> void
{
    for (std::size_t i = 0; i < spline_indices.size(); i++)
        spline_indices[i] = static_cast<float>(std::fmod((spline_indices[i] + delta_time), mesh_spline_pairs[i].second.spline_points.size()));

    const auto view_matrix = camera.transform.get_view_matrix();
    const auto right       = glm::vec3{view_matrix[0][0], view_matrix[1][0], view_matrix[2][0]};
    const auto up          = glm::vec3{view_matrix[0][1], view_matrix[1][1], view_matrix[2][1]};
    const auto forward     = -glm::vec3{view_matrix[0][2], view_matrix[1][2], view_matrix[2][2]};

    auto translation = glm::vec3{};
    if (buttons[GLFW_KEY_W])
        translation += forward * delta_time * camera.movement_speed;

    if (buttons[GLFW_KEY_S])
        translation -= forward * delta_time * camera.movement_speed;

    if (buttons[GLFW_KEY_D])
        translation += right * delta_time * camera.movement_speed;

    if (buttons[GLFW_KEY_A])
        translation -= right * delta_time * camera.movement_speed;

    if (buttons[GLFW_KEY_Q])
        translation += up * delta_time * camera.movement_speed;

    if (buttons[GLFW_KEY_E])
        translation -= up * delta_time * camera.movement_speed;

    camera.transform.position = translate(glm::mat4{1.0F}, translation) * camera.transform.position;
    if (x_offset != 0.0F) {
        camera.transform.rotation.y -= x_offset * camera.mouse_sensitivity;
        camera.transform.rotation.y = std::fmod(camera.transform.rotation.y, 360.0F);
        x_offset                    = 0.0F;
    }

    if (y_offset != 0.0F) {
        camera.transform.rotation.x -= y_offset * camera.mouse_sensitivity;
        camera.transform.rotation.x = std::clamp(camera.transform.rotation.x, -89.0F, 89.0F);
        y_offset                    = 0.0F;
    }
}

auto mouse_callback(GLFWwindow * /*window*/, double x_pos, double y_pos) -> void
{
    if (first_mouse) {
        x           = static_cast<float>(x_pos);
        y           = static_cast<float>(y_pos);
        first_mouse = false;
    }

    x_offset = static_cast<float>(x_pos) - x;
    y_offset = static_cast<float>(y_pos) - y;

    x = static_cast<float>(x_pos);
    y = static_cast<float>(y_pos);
}

auto main() -> int
{
    if (glfwInit() == 0) {
        std::cout << "failed to initialize glfw" << std::endl;
        std::exit(-1);
    }

    glfwWindowHint(GLFW_RESIZABLE, 0);

    const auto window = glfwCreateWindow(width, height, "b spline", nullptr, nullptr);
    if (window == nullptr) {
        std::cout << "failed to create window" << std::endl;
        std::exit(-1);
    }

    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetKeyCallback(window, key_callback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);

    if (glewInit() != GLEW_OK) {
        std::cout << "failed to initialize glew" << std::endl;
        std::exit(-1);
    }

    glDisable(GL_DEPTH_TEST);
    glViewport(0, 0, width, height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    const auto projection_matrix = camera.projection;
    glLoadMatrixf(&projection_matrix[0][0]);

    glClearColor(1.0F, 1.0F, 1.0F, 1.0F);

    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glCullFace(GL_BACK);

    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();

        delta_time = std::chrono::duration<float, std::milli>(std::chrono::high_resolution_clock::now() - now).count();
        now        = std::chrono::high_resolution_clock::now();
        process_input(delta_time);

        glClear(GL_COLOR_BUFFER_BIT);
        display();

        glfwSwapBuffers(window);
    }

    glfwDestroyWindow(window);
    glfwTerminate();
}
