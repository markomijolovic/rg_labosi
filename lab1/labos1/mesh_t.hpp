#pragma once

#include <fstream>
#include <glm/glm.hpp>
#include <sstream>
#include <string>
#include <vector>

struct mesh_t {
    std::vector<glm::vec3>  vertices{};
    std::vector<glm::ivec3> faces{};
};

[[nodiscard]] inline mesh_t load_mesh(const std::string &path)
{
    mesh_t mesh{};

    auto ifstream = std::ifstream{path};
    auto line     = std::string{};

    while (std::getline(ifstream, line)) {
        auto line_stream = std::stringstream{line};
        auto ch          = char{};
        line_stream >> ch;
        if (line[0] == 'v') {
            auto vertex = glm::vec3{};
            line_stream >> vertex.x >> vertex.y >> vertex.z;
            mesh.vertices.push_back(vertex);
        } else if (line[0] == 'f') {
            auto face = glm::ivec3{};
            line_stream >> face.x >> face.y >> face.z;
            mesh.faces.push_back(face);
        }
    }

    return mesh;
}
