#pragma once

#include <glm/ext/matrix_transform.hpp>
#include <glm/glm.hpp>

struct transform_t {
    glm::vec4 position{0.0F, 0.0F, 0.0F, 1.0F};
    glm::vec3 rotation{};
    glm::vec3 scale{1.0F, 1.0F, 1.0F};

    [[nodiscard]] auto get_transform_matrix() const -> glm::mat4x4
    {
        auto transform_matrix = glm::translate(glm::mat4{1.0F}, glm::vec3{position});
        transform_matrix      = glm::rotate(transform_matrix, glm::radians(rotation.z), {0.0F, 0.0F, 1.0F});
        transform_matrix      = glm::rotate(transform_matrix, glm::radians(rotation.y), {0.0F, 1.0F, 0.0F});
        transform_matrix      = glm::rotate(transform_matrix, glm::radians(rotation.x), {1.0F, 0.0F, 0.0F});

        return glm::scale(transform_matrix, scale);
    }
    [[nodiscard]] auto get_view_matrix() const -> glm::mat4x4
    {
        auto transform_matrix = glm::rotate(glm::mat4{1.0F}, glm::radians(-rotation.x), {1.0F, 0.0F, 0.0F});
        transform_matrix      = glm::rotate(transform_matrix, glm::radians(-rotation.y), {0.0F, 1.0F, 0.0F});

        return glm::translate(transform_matrix, -glm::vec3{position});
    }
};

struct camera_t {
    glm::mat4x4 projection{};
    transform_t transform{};
    float       movement_speed{0.01F};
    float       mouse_sensitivity{0.1F};
};
